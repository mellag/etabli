#!/usr/bin/env python
#
# This script just walk accross your imap mailbox and show size+number of email on sub directories.
# Use by default your USER name onto the UGA zimbra server.
# 
# Authors: 
#  - G. Mella
# License : Copyleft 

import optparse
import sys
import imaplib, getpass,string
import getpass

parser = optparse.OptionParser()
parser.add_option('-s', '--server', dest="server", default="zimbra.univ-grenoble-alpes.fr",)
parser.add_option('-u', '--user', dest="user", default=getpass.getuser()+"@univ-grenoble-alpes.fr",)
parser.add_option('--version', dest="version", default=1.0, type="float",)

options, remainder = parser.parse_args()


imaplib._MAXLINE = 500000
M = imaplib.IMAP4_SSL(options.server) # put your server here...
print 'user    :', options.user
M.login(options.user, getpass.getpass())

# The list of all folders
result,list = M.list()

print "%-30s I%5s I%-50s" % ("Size", "# Msg", "Folder")

number_of_messages_all = 0
size_all = 0

for item in list[:]:
    x = item.split()
    mailbox = string.join(x[2:])

    try:
        # Select the desired folder
        result, number_of_messages = M.select(mailbox, readonly=1)
        number_of_messages_all += int(number_of_messages[0])

        size_folder = 0
        # Go through all the messages in the selected folder
        typ, msg = M.search(None, 'ALL')
        # Find the first and last messages
        m = [int(x) for x in msg[0].split()]
        m.sort()
        if m:
            message_set = "%d:%d" % (m[0], m[-1])
            result, sizes_response = M.fetch(message_set, "(UID RFC822.SIZE)")
            for i in range(m[-1]):
                tmp = sizes_response[i].split()
                size_folder += int(tmp[-1].replace(')', ''))
            else:
                print( "%-30s I%5d I%-50s" % ( size_folder, int(number_of_messages[0]),mailbox))
                size_all += size_folder
                size_folder = 0
    except:
        print ("%-30s I%5s I%-50s" % ("??", "??", mailbox))

print ("\n%-30s I%5i I%50s \n" % ("Sum", number_of_messages_all, size_all))

# Close the connection
M.logout()


Quelques liens pour illustrer mon retour:

# Plus
* depots publics simples à mettre en oeuvre/utiliser :  cf https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC
   * [ ] Findable : indéxé par les moteurs de recherches
   * [ ] Accessible : anywhere anytime sans VPN depuis ou en dehors du réseau UGA
   * [ ] Interoperable : ( pas trouvé... Q: j'peux avoir un merge request sur un snippet ? ptetre le mode remote urm pour synchroniser sur d'autres plateformes... )
   * [ ] Reusable : cloned cloned cloned (MR/PR welcomes)
   
* Construction d'Images docker pour certains services avec gitlab-ci
   * stockage en registry locale - utilisation d'un runner du [groupe OSUG](https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/jmmc-jmdcui-docker/-/settings/ci_cd) avec support [dind](https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/jmmc-jmdcui-docker/blob/master/.gitlab-ci.yml)
* markdown amélioré : support de mermaid par ex: 
   * [ ] [Diagram](https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/jmmc-jmdcui-docker#quick-collaboration-diagram)
   * [ ] [Gant](https://gricad-gitlab.univ-grenoble-alpes.fr/mellag/etabli/blob/master/README.md)

# Moins
* possible de mettre du code protégé (pas FAIR / mais pas le problème gitlab en fait! )  ;)
* [issue non désirée] (https://gricad-gitlab.univ-grenoble-alpes.fr/OSUG/JMMC/jmmc-ees17-school-tutorials/issues/1) Je dois mal m'y prendre dans les RÉGLAGES ?
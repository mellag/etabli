#!/bin/bash
#
# Read a list of vlan and walk through every ips inside.
# Output oneline per vlan and one line per ip that get a DNS record (in dokuwiki format)
#
# I like in this script:
# - the computation of the size of a given vlan given to it's network address

#
#Plages  Nom du VLAN
VLANS="129.88.191.0/24   SPR-OSUG
129.88.192.0/24   SPR-OSUG-DC
129.88.193.0/27   SPR-IPAG
152.77.134.32/27  VM-OSUG-DC-06
152.77.134.128/29   VM-OSUG-DC-01
152.77.134.136/29   VM-OSUG-DC-02
152.77.134.144/29   VM-OSUG-DC-03
152.77.134.176/28   VM-OSUG-ISTERRE
152.77.134.192/26   VM-OSUG-IPAG
152.77.135.56/30  VM-OSUG-OWNCLOUD
152.77.135.60/30  VM-OSUG-LDAP
152.77.135.100/30   VM-OSUG-URBASIS
152.77.135.104/30   VM-OSUG-MONITORING
152.77.135.144/28   VM-OSUG-DC-INFRA
152.77.135.160/27   VM-OSUG-DC-04
152.77.135.192/27   VM-OSUG-DC-05"
#VLANS="152.77.114.192/26 VLAN OSUG-SRV"
#VLANS="152.77.135.192/27   VM-OSUG-DC-05"

skipExpr="x670-bio-cermo\|spring-tn-osug" 

handleIp(){
  IP=$1
  
  if HOSTR=$(host $IP)
  then
    HOST=${HOSTR##* }
    if ping -c 1 $IP &>/dev/null
    then
      PING="ping"
    else
      PING=""
    fi
    
    if curl -m 1 --retry-max-time 0 --connect-timeout 1 ftp://$IP &>/dev/null
    then
      FTP="ftp"
    else
      FTP=""
    fi
 
    
    if curl -q -m 1 --retry-max-time 0 --connect-timeout 1 http://$IP &> /dev/null
    then
      HTTP="http"
    else
      HTTP=""
    fi

   
    curl -v -q -m 1 --retry-max-time 0 --connect-timeout 1 https://$IP &> testHTTPS
    EXPIRE=$(grep expire testHTTPS)
    if [ -n "$EXPIRE" ]  
    then
      HTTPS="https(${EXPIRE#*:})"
    else
      HTTPS=""
    fi
    rm testHTTPS




    if ! echo $HOST | grep $skipExpr &> /dev/null
    then
      echo "|$IP|$HOST| $PING $FTP $HTTP $HTTPS|"
    fi
  fi
}

handleVlan(){
  ADDRESS=$1
  NAME=$2

  LPART=${ADDRESS%%/*}
  NETSIZE=${ADDRESS##*/}
  NETWORKPREFIX=${LPART%.*}
  NETWORKSUFFIX=${LPART##*.}
  let FROM="$NETWORKSUFFIX"
  let TO="$FROM+2**(32-$NETSIZE)-2"
  echo -e "^VLAN $NAME | $NETWORKPREFIX.$FROM -> $NETWORKPREFIX.$TO | Note |"
  for IPNUM in $(seq $FROM $TO) 
  do
    IP=$NETWORKPREFIX.$IPNUM
    handleIp $IP
  done

}

echo "$VLANS" | while read vlan ; do handleVlan $vlan; done

echo -e "\n\nNote: Filtered names using : '$skipExpr'"

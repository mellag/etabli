{
  "parameters": [
    {
      "default": false,
      "label": "Perform alignment with BCD in position OUT?",
      "minihelp": "Perform alignment with BCD in position OUT?",
      "name": "SEQ.ALIGN.BCDOUT",
      "type": "boolean"
    },
    {
      "allowedValues": {
        "values": [
          "L",
          "N"
        ]
      },
      "default": "L",
      "label": "Band L or N",
      "name": "SEQ.BAND",
      "type": "string"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 60,
            "min": 0
          }
        ]
      },
      "default": 0.8,
      "label": "Integration time for image",
      "minihelp": "User defined integration time in seconds (0..60).",
      "name": "SEQ.IMG.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10000,
            "min": 1
          }
        ]
      },
      "default": 1,
      "label": "Number of integrations for image",
      "minihelp": "Number of integrations for each exposure",
      "name": "SEQ.IMG.NDIT",
      "type": "integer"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 60,
            "min": 0
          }
        ]
      },
      "default": 0.8,
      "label": "Integration time for reference image",
      "minihelp": "User defined integration time in seconds (0..60).",
      "name": "SEQ.IMG.REF.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10000,
            "min": 1
          }
        ]
      },
      "default": 1,
      "label": "Number of integrations for reference image",
      "minihelp": "Number of integrations for each exposure",
      "name": "SEQ.IMG.REF.NDIT",
      "type": "integer"
    },
    {
      "allowedValues": {
        "values": [
          "A",
          "C"
        ]
      },
      "default": "A",
      "label": "Alignment Mode",
      "minihelp": "Alignment Mode (A = align / C = check)",
      "name": "SEQ.MODE",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 60,
            "min": 0
          }
        ]
      },
      "default": 0.8,
      "label": "Integration time for pupil",
      "minihelp": "User defined integration time in seconds (0..60).",
      "name": "SEQ.PUP.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10000,
            "min": 1
          }
        ]
      },
      "default": 1,
      "label": "Number of integrations for pupil",
      "minihelp": "Number of integrations for each exposure",
      "name": "SEQ.PUP.NDIT",
      "type": "integer"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 60,
            "min": 0
          }
        ]
      },
      "default": 0.8,
      "label": "Integration time for reference pupil",
      "minihelp": "User defined integration time in seconds (0..60).",
      "name": "SEQ.PUP.REF.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10000,
            "min": 1
          }
        ]
      },
      "default": 1,
      "label": "Number of integrations for reference pupil",
      "minihelp": "Number of integrations for each exposure",
      "name": "SEQ.PUP.REF.NDIT",
      "type": "integer"
    },
    {
      "default": true,
      "label": "Search reference position",
      "minihelp": "Search reference position (T/F)?",
      "name": "SEQ.REF.SEARCH",
      "type": "boolean"
    },
    {
      "allowedValues": {
        "values": [
          "TECHNICAL",
          "SCIENCE",
          "CALIB",
          "TEST"
        ]
      },
      "default": "TECHNICAL",
      "label": "Data product",
      "name": "DPR.CATG",
      "type": "keyword"
    }
  ],
  "templateName": "MATISSE_gen_tec_ali",
  "type": "calib"
}

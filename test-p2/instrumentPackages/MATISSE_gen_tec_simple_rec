{
  "parameters": [
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 60,
            "min": 0
          }
        ]
      },
      "default": 0.02,
      "label": "Integration time for L&M detector",
      "minihelp": "User defined integration time for L&M detector in seconds",
      "name": "DET1.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10000,
            "min": 1
          }
        ]
      },
      "default": 40,
      "label": "Number of frames for L-band",
      "minihelp": "Number of frames for each exposure for L-band",
      "name": "DET1.NDIT",
      "type": "integer"
    },
    {
      "allowedValues": {
        "values": [
          "SCI-FAST-SPEED",
          "SCI-SLOW-SPEED",
          "AUTO"
        ]
      },
      "default": "SCI-FAST-SPEED",
      "label": "Frame mode for L&M detector",
      "minihelp": "Frame mode for L detector  (FAST should only be used on very bright targets)",
      "name": "DET1.READ.CURNAME",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 60,
            "min": 0
          }
        ]
      },
      "default": 0.02,
      "label": "Integration time for N-band",
      "minihelp": "User defined integration time for N-band in seconds",
      "name": "DET2.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10000,
            "min": 1
          }
        ]
      },
      "default": 40,
      "label": "Number of frames for N-band",
      "minihelp": "Number of frames for each exposure for N-band",
      "name": "DET2.NDIT",
      "type": "integer"
    },
    {
      "allowedValues": {
        "values": [
          "L",
          "N"
        ]
      },
      "default": "L",
      "label": "Band L or N",
      "name": "SEQ.BAND",
      "type": "string"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 4.9,
            "min": 2.9
          }
        ]
      },
      "default": 3.5,
      "label": "Central wavelength for L&M bands",
      "minihelp": "Central wavelength [um] for LM-band  (only used if readout mode is SCI-SLOW-SPEED)",
      "name": "SEQ.DIL.WL0",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "AUTOTEST",
          "SI-PHOT",
          "HI-SENS"
        ]
      },
      "default": "AUTOTEST",
      "label": "Instrument mode (AUTOTEST SI-PHOT HI-SENS)",
      "minihelp": "Instrument mode (AUTOTEST SI-PHOT HI-SENS)",
      "name": "SEQ.INS.MODE",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 100,
            "min": 0
          }
        ]
      },
      "default": 1,
      "label": "Number of Exposures",
      "minihelp": "Number of Exposures",
      "name": "SEQ.NEXP",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "S",
          "T"
        ]
      },
      "default": "T",
      "label": "Sky (S) or Target (T)",
      "minihelp": "Sky (S) or Target (T)",
      "name": "SEQ.OBS.TYPE",
      "type": "keyword"
    },
    {
      "default": false,
      "label": "Do instrument setup?",
      "minihelp": "Do instrument setup (T/F)?",
      "name": "SEQ.SETUP",
      "type": "boolean"
    },
    {
      "allowedValues": {
        "values": [
          "HIGH+",
          "HIGH",
          "OPEN",
          "MED",
          "PUPIL",
          "LOW"
        ]
      },
      "default": "LOW",
      "label": "Dispersive element L and M",
      "minihelp": "Dispersive element L and M",
      "name": "INS.DIL.NAME",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "OPEN",
          "HIGH",
          "FREE1",
          "FREE2",
          "PUPIL",
          "LOW"
        ]
      },
      "default": "LOW",
      "label": "Dispersive element N",
      "minihelp": "Dispersive element N",
      "name": "INS.DIN.NAME",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "OPEN",
          "LM",
          "L",
          "SPCAL",
          "FLAT",
          "M",
          "L-"
        ]
      },
      "default": "LM",
      "label": "Spectral filter L and M",
      "minihelp": "Spectral filter L and M",
      "name": "INS.FIL.NAME",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "PHOTO",
          "INTER"
        ]
      },
      "default": "PHOTO",
      "label": "Photometric slider L and M",
      "minihelp": "Photometric slider L and M",
      "name": "INS.PIL.NAME",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "PHOTO",
          "INTER"
        ]
      },
      "default": "INTER",
      "label": "Photometric slider N",
      "minihelp": "Photometric slider N",
      "name": "INS.PIN.NAME",
      "type": "keyword"
    }
  ],
  "templateName": "MATISSE_gen_tec_simple_rec",
  "type": "test"
}

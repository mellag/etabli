{
  "parameters": [
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 25,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "AcqCam guide star magnitude in H",
      "minihelp": "AcqCam guide star magnitude in H for lab field guiding",
      "name": "SEQ.FI.HMAG",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "AUTO",
          "1",
          "2",
          "7",
          "9"
        ]
      },
      "default": "AUTO",
      "label": "FringeTracker mode",
      "minihelp": "FringeTracker mode (AUTO 1 2 7 9)",
      "name": "SEQ.FT.MODE",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 300,
            "min": 0
          }
        ]
      },
      "default": 0,
      "label": "FT object diameter (mas)",
      "minihelp": "FT object diameter (mas); Required only for calibrator",
      "name": "SEQ.FT.ROBJ.DIAMETER",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 30,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "FT object total magnitude",
      "minihelp": "FT object total magnitude in K (-10..30)",
      "name": "SEQ.FT.ROBJ.MAG",
      "type": "number"
    },
    {
      "default": "Name",
      "label": "FT object name",
      "minihelp": "FT object name",
      "name": "SEQ.FT.ROBJ.NAME",
      "type": "string"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 1,
            "min": 0
          }
        ]
      },
      "default": 1,
      "label": "FT object expected visibility",
      "minihelp": "FT object expected visibility",
      "name": "SEQ.FT.ROBJ.VIS",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 7000,
            "min": -7000
          }
        ]
      },
      "default": 0,
      "label": "RA offset from FT to SC object (mas)",
      "minihelp": "RA offset from FT to SC object (mas) : (-7000..7000)",
      "name": "SEQ.INS.SOBJ.X",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 7000,
            "min": -7000
          }
        ]
      },
      "default": 0,
      "label": "DEC offset from FT to SC object (mas)",
      "minihelp": "DEC offset from FT to SC object (mas) : (-7000..7000)",
      "name": "SEQ.INS.SOBJ.Y",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 8000,
            "min": -8000
          }
        ]
      },
      "default": 2000,
      "label": "Sky dRA offset in milliarcsecond",
      "minihelp": "Sky dRA offset in milliarcsecond (-8000..8000)",
      "name": "SEQ.SKY.X",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 8000,
            "min": -8000
          }
        ]
      },
      "default": 2000,
      "label": "Sky dDEC offset in milliarcsecond",
      "minihelp": "Sky dDEC offset in milliarcsecond (-8000..8000)",
      "name": "SEQ.SKY.Y",
      "type": "number"
    },
    {
      "default": 3000,
      "label": "L band flux in Jy",
      "minihelp": "L band flux in Jy",
      "name": "SEQ.TARG.FLUX.L",
      "type": "number"
    },
    {
      "default": 300,
      "label": "N band flux in Jy",
      "minihelp": "N band flux in Jy",
      "name": "SEQ.TARG.FLUX.N",
      "type": "number"
    },
    {
      "default": 0,
      "label": "K band magnitude",
      "minihelp": "K band magnitude",
      "name": "SEQ.TARG.MAG.K",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 15,
            "min": -15
          }
        ]
      },
      "default": 0,
      "label": "Differential tracking in RA",
      "minihelp": "Alpha additional tracking velocity in arcseconds/s (-15 to 15).",
      "name": "TEL.TARG.ADDVELALPHA",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 15,
            "min": -15
          }
        ]
      },
      "default": 0,
      "label": "Differential tracking in DEC",
      "minihelp": "Delta additional tracking velocity in arcseconds/s (-15 to 15).",
      "name": "TEL.TARG.ADDVELDELTA",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "IN",
          "OUT"
        ]
      },
      "default": "IN",
      "label": "Fringe-tracker spectrometer Wollaston",
      "minihelp": "Fringe-tracker spectrometer polarisation mode; split (IN) or combined (OUT)",
      "name": "INS.FT.POL",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "IN",
          "OUT"
        ]
      },
      "default": "IN",
      "label": "Science spectrometer Wollaston",
      "minihelp": "Science spectrometer polarisation mode; split (IN) or combined (OUT)",
      "name": "INS.SPEC.POL",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "LOW",
          "MED",
          "HIGH"
        ]
      },
      "default": "MED",
      "label": "Science spectrometer resolution",
      "minihelp": "Science spectrometer resolution (LOW MED HIGH)",
      "name": "INS.SPEC.RES",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "ra"
        ]
      },
      "default": "0.",
      "label": "RA of guide star if COU guide star is SETUPFILE",
      "minihelp": "Coude guide star alpha as HHMMSS.TTT (0 to 240000) if not SCIENCE",
      "name": "COU.AG.ALPHA",
      "type": "coord"
    },
    {
      "allowedValues": {
        "values": [
          "dec"
        ]
      },
      "default": "0.",
      "label": "DEC of guide star if COU guide star is SETUPFILE",
      "minihelp": "Coude guide star delta as +-DDMMSS.TTT (-900000 to 900000) if not SCIENCE",
      "name": "COU.AG.DELTA",
      "type": "coord"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 3000,
            "min": -2000
          }
        ]
      },
      "default": 2000,
      "label": "Epoch",
      "minihelp": "Epoch expressed as year",
      "name": "COU.AG.EPOCH",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 3000,
            "min": -2000
          }
        ]
      },
      "default": 2000,
      "label": "Equinox",
      "minihelp": "Equinox expressed as year",
      "name": "COU.AG.EQUINOX",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "NONE",
          "SETUPFILE",
          "SCIENCE"
        ]
      },
      "default": "SCIENCE",
      "label": "COU guide star",
      "minihelp": "In case of SCIENCE guide star is science target",
      "name": "COU.AG.GSSOURCE",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "Off-axis Coude Proper Motion Alpha",
      "minihelp": "Off-axis Coude Guide Star Proper Motion Alpha in arcseconds/year",
      "name": "COU.AG.PMA",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "Off-axis Coude Proper Motion Delta",
      "minihelp": "Off-axis Coude Guide Star Proper Motion Delta in arcseconds/year",
      "name": "COU.AG.PMD",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "DEFAULT",
          "NONE",
          "AUTO_GUID",
          "ADAPT_OPT",
          "ADAPT_OPT_TCCD"
        ]
      },
      "default": "ADAPT_OPT",
      "label": "Guiding Type",
      "minihelp": "Not all types are available on all types of telescopes",
      "name": "COU.AG.TYPE",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 25,
            "min": 0
          }
        ]
      },
      "default": 0,
      "label": "GS mag in V",
      "minihelp": "Coude guide star magnitude as seen by Coude guide star sensor",
      "name": "COU.GS.MAG",
      "type": "number"
    }
  ],
  "templateName": "MATISSE_img_acq_ft",
  "type": "acquisition"
}

{
  "parameters": [
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 7,
            "min": 0.7
          }
        ]
      },
      "default": 0.7,
      "label": "AcqCam integration time (DIT in s)",
      "minihelp": "AcqCam integration time (DIT in s) (0.7..7)",
      "name": "DET1.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 100,
            "min": 1
          }
        ]
      },
      "default": 1,
      "label": "Number of AcqCam frames (NDIT)",
      "minihelp": "Number of AcqCam frames (NDIT) (1..100)",
      "name": "DET1.NDIT",
      "type": "integer"
    },
    {
      "allowedValues": {
        "values": [
          "0.3",
          "1",
          "3",
          "5",
          "10",
          "30",
          "60"
        ]
      },
      "default": "0.3",
      "label": "Science integration time (DIT in s)",
      "minihelp": "Science integration time (DIT in s) (0.3 1 3 5 10 30 60)",
      "name": "DET2.DIT",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 300,
            "min": 10
          }
        ]
      },
      "default": 256,
      "label": "Number of science frames (NDIT)",
      "minihelp": "Number of science frames (NDIT) (10..300)",
      "name": "DET2.NDIT",
      "type": "integer"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 100,
            "min": 1
          }
        ]
      },
      "default": 2,
      "label": "Number of exposures",
      "name": "SEQ.NEXPO",
      "type": "integer"
    },
    {
      "default": true,
      "label": "FDDL step mode",
      "minihelp": "FDDLs move only after file recorded on (T) or off (F)",
      "name": "SEQ.STEPMODE",
      "type": "boolean"
    },
    {
      "default": 1,
      "label": "Number of FT scanning periods",
      "minihelp": "Number of scanning periods to record for FT wavelength calibration",
      "name": "INS.FDDL.NPERIOD",
      "type": "number"
    },
    {
      "default": 1,
      "label": "Number of SC scanning periods",
      "minihelp": "Number of scanning periods to record for SC wavelength calibration",
      "name": "INS.FDDL.NPERIOD_SC",
      "type": "number"
    },
    {
      "default": 50,
      "label": "FDDL scanning amplitude SC",
      "minihelp": "FDDL scanning amplitude for SC wavelength calibration (micron)",
      "name": "INS.FDDL.RANGE_SC",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "IN",
          "OUT"
        ]
      },
      "default": "OUT",
      "label": "Fringe-tracker spectrometer Wollaston",
      "minihelp": "Fringe-tracker spectrometer polarisation mode: split (IN) or combined (OUT)",
      "name": "INS.FILT1.NAME",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "LOW",
          "MED",
          "HIGH",
          "SAME"
        ]
      },
      "default": "LOW",
      "label": "SC spectrometer resolution",
      "minihelp": "SC spectrometer resolution (LOW MED HIGH SAME)",
      "name": "INS.FILT2.NAME",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "IN",
          "OUT"
        ]
      },
      "default": "OUT",
      "label": "Science spectrometer Wollaston",
      "minihelp": "Science spectrometer polarisation mode: split (IN) or combined (OUT)",
      "name": "INS.FILT3.NAME",
      "type": "keyword"
    },
    {
      "default": 1.65,
      "label": "FT lamp intensity",
      "minihelp": "FT lamp intensity (Volts)",
      "name": "INS.LAMP4.INTENS1",
      "type": "number"
    },
    {
      "default": 1.23,
      "label": "SC lamp intensity",
      "minihelp": "SC lamp intensity (Volts)",
      "name": "INS.LAMP4.INTENS2",
      "type": "number"
    }
  ],
  "templateName": "GRAVITY_gen_cal_waveSC",
  "type": "calib"
}

{
  "parameters": [
    {
      "allowedValues": {
        "values": [
          "L",
          "N"
        ]
      },
      "default": "L",
      "label": "Band L or N",
      "name": "SEQ.BAND",
      "type": "string"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 4,
            "min": 1
          }
        ]
      },
      "default": [
        1,
        2,
        3,
        4
      ],
      "label": "List of beams",
      "minihelp": "List of beams",
      "name": "SEQ.BEAM.LIST",
      "type": "numlist"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 60,
            "min": 0
          }
        ]
      },
      "default": 0.8,
      "label": "Integration time for image",
      "minihelp": "User defined integration time in seconds (0..60).",
      "name": "SEQ.IMG.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10000,
            "min": 1
          }
        ]
      },
      "default": 1,
      "label": "Number of integrations for image",
      "minihelp": "Number of integrations for each exposure",
      "name": "SEQ.IMG.NDIT",
      "type": "integer"
    },
    {
      "default": "SCI-FAST-SPEED",
      "label": "Frame mode for L or N detector",
      "minihelp": "Frame mode for L or N detector",
      "name": "SEQ.IMG.READ.CURNAME",
      "type": "string"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 1000,
            "min": 1
          }
        ]
      },
      "default": [
        100,
        5,
        3,
        10
      ],
      "label": "List of moves in pixels to validate the periscopes matrix",
      "minihelp": "List of moves in pixels to validate the periscopes matrix (order is: IMX, IMY, PUPX, PUPY)",
      "name": "SEQ.PIX.TEST.LIST",
      "type": "numlist"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 60,
            "min": 0
          }
        ]
      },
      "default": 0.8,
      "label": "Integration time for pupil",
      "minihelp": "User defined integration time in seconds (0..60).",
      "name": "SEQ.PUP.DIT",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10000,
            "min": 1
          }
        ]
      },
      "default": 1,
      "label": "Number of integrations for pupil",
      "minihelp": "Number of integrations for each exposure",
      "name": "SEQ.PUP.NDIT",
      "type": "integer"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 30000,
            "min": 1
          }
        ]
      },
      "default": [
        50,
        50,
        10000,
        10000
      ],
      "label": "List of steps to calibrate the periscopes",
      "minihelp": "List of steps [enc] to calibrate the periscopes (order is: TP, TL, LY, LX)",
      "name": "SEQ.STEP.LIST",
      "type": "numlist"
    },
    {
      "allowedValues": {
        "values": [
          "TECHNICAL",
          "SCIENCE",
          "CALIB",
          "TEST"
        ]
      },
      "default": "TECHNICAL",
      "label": "Data product",
      "name": "DPR.CATG",
      "type": "keyword"
    }
  ],
  "templateName": "MATISSE_gen_tec_periscopes",
  "type": "calib"
}

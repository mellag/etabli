{
  "parameters": [
    {
      "default": "Name",
      "label": "FT object name",
      "minihelp": "FT object name",
      "name": "SEQ.FT.ROBJ.NAME",
      "type": "string"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 30,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "FT object total magnitude",
      "minihelp": "FT object total magnitude in K (-10..30)",
      "name": "SEQ.FT.ROBJ.MAG",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 300,
            "min": 0
          }
        ]
      },
      "default": 0,
      "label": "FT object diameter (mas)",
      "minihelp": "FT object diameter (mas); Required only for calibrator",
      "name": "SEQ.FT.ROBJ.DIAMETER",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 1,
            "min": 0
          }
        ]
      },
      "default": 1,
      "label": "FT object expected visibility",
      "minihelp": "FT object expected visibility",
      "name": "SEQ.FT.ROBJ.VIS",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "AUTO",
          "1",
          "2",
          "7",
          "9"
        ]
      },
      "default": "AUTO",
      "label": "FringeTracker mode",
      "minihelp": "FringeTracker mode (AUTO 1 2 7 9)",
      "name": "SEQ.FT.MODE",
      "type": "keyword"
    },
    {
      "default": "Name",
      "label": "SC object name",
      "minihelp": "SC object name",
      "name": "SEQ.INS.SOBJ.NAME",
      "type": "string"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 30,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "SC object total magitude",
      "minihelp": "SC object total magnitude in K (-10..30)",
      "name": "SEQ.INS.SOBJ.MAG",
      "type": "number"
    },
    {
      "default": 0,
      "label": "SC object diameter (mas)",
      "minihelp": "SC object diameter (mas); Required only for calibrator",
      "name": "SEQ.INS.SOBJ.DIAMETER",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 1,
            "min": 0
          }
        ]
      },
      "default": 1,
      "label": "SC object expected visibility",
      "minihelp": "SC object expected visibility",
      "name": "SEQ.INS.SOBJ.VIS",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 7000,
            "min": -7000
          }
        ]
      },
      "default": 0,
      "label": "RA offset from FT to SC object (mas)",
      "minihelp": "RA offset from FT to SC object (mas) : (-7000..7000)",
      "name": "SEQ.INS.SOBJ.X",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 7000,
            "min": -7000
          }
        ]
      },
      "default": 0,
      "label": "DEC offset from FT to SC object (mas)",
      "minihelp": "DEC offset from FT to SC object (mas) : (-7000..7000)",
      "name": "SEQ.INS.SOBJ.Y",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 20000,
            "min": -20000
          }
        ]
      },
      "default": 0,
      "label": "FT object dRA offset",
      "minihelp": "RA offset of FT object to previous FT object in milliarcsecond:(-20000..20000)",
      "name": "SEQ.DITHER.X",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 20000,
            "min": -20000
          }
        ]
      },
      "default": 0,
      "label": "FT object dDEC offset",
      "minihelp": "DEC offset of FT object to previous FT object in milliarcsecond:(-20000..20000)",
      "name": "SEQ.DITHER.Y",
      "type": "number"
    }
  ],
  "templateName": "GRAVITY_dual_acq_dither",
  "type": "acquisition"
}

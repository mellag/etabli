{
  "parameters": [
    {
      "allowedValues": {
        "values": [
          "AUTO",
          "1",
          "2",
          "7",
          "9"
        ]
      },
      "default": "AUTO",
      "label": "FringeTracker mode",
      "minihelp": "FringeTracker mode (AUTO 1 2 7 9)",
      "name": "SEQ.FT.MODE",
      "type": "keyword"
    },
    {
      "default": "Name",
      "label": "SC object name",
      "minihelp": "SC object name",
      "name": "SEQ.INS.SOBJ.NAME",
      "type": "string"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 30,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "SC object total magitude",
      "minihelp": "SC object total magnitude in K (-10..30)",
      "name": "SEQ.INS.SOBJ.MAG",
      "type": "number"
    },
    {
      "default": 0,
      "label": "SC object diameter (mas)",
      "minihelp": "SC object diameter (mas); Required only for calibrator",
      "name": "SEQ.INS.SOBJ.DIAMETER",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 1,
            "min": 0
          }
        ]
      },
      "default": 1,
      "label": "SC object expected visibility",
      "minihelp": "SC object expected visibility",
      "name": "SEQ.INS.SOBJ.VIS",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 25,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "AcqCam guide star magnitude in H",
      "minihelp": "AcqCam guide star magnitude in H for lab field guiding",
      "name": "SEQ.FI.HMAG",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 20,
            "min": -20
          }
        ]
      },
      "default": 0,
      "label": "SC object parallax (arcseconds)",
      "minihelp": "SC object parallax (arcseconds)",
      "name": "TEL.TARG.PARALLAX",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "LOW",
          "MED",
          "HIGH"
        ]
      },
      "default": "MED",
      "label": "Science spectrometer resolution",
      "minihelp": "Science spectrometer resolution (LOW MED HIGH)",
      "name": "INS.SPEC.RES",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "IN",
          "OUT"
        ]
      },
      "default": "IN",
      "label": "Fringe-tracker spectrometer Wollaston",
      "minihelp": "Fringe-tracker spectrometer polarisation mode; split (IN) or combined (OUT)",
      "name": "INS.FT.POL",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "IN",
          "OUT"
        ]
      },
      "default": "IN",
      "label": "Science spectrometer Wollaston",
      "minihelp": "Science spectrometer polarisation mode; split (IN) or combined (OUT)",
      "name": "INS.SPEC.POL",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "SCIENCE",
          "SETUPFILE"
        ]
      },
      "default": "SCIENCE",
      "label": "Coude guide star (GS) input",
      "minihelp": "If guide star is science then choose SCIENCE else SETUPFILE",
      "name": "COU.AG.GSSOURCE",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "ra"
        ]
      },
      "default": "00:00:00.000",
      "label": "GS RA if SETUPFILE",
      "minihelp": "Coude guide star alpha as HH:MM:SS.TTT if not SCIENCE",
      "name": "COU.AG.ALPHA",
      "type": "coord"
    },
    {
      "allowedValues": {
        "values": [
          "dec"
        ]
      },
      "default": "00:00:00.000",
      "label": "GS DEC if SETUPFILE",
      "minihelp": "Coude guide star delta as +-DD:MM:SS.TTT if not SCIENCE",
      "name": "COU.AG.DELTA",
      "type": "coord"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 25,
            "min": 0
          }
        ]
      },
      "default": 0,
      "label": "GS magnitude",
      "minihelp": "Coude guide star magnitude",
      "name": "COU.GS.MAG",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "GS proper motion in RA",
      "minihelp": "GS proper Motion Alpha in arcseconds/year (-10.0 to 10.0).",
      "name": "COU.AG.PMA",
      "type": "number"
    },
    {
      "allowedValues": {
        "ranges": [
          {
            "max": 10,
            "min": -10
          }
        ]
      },
      "default": 0,
      "label": "GS proper motion in DEC",
      "minihelp": "GS proper Motion Delta in arcseconds/year (-10.0 to 10.0)",
      "name": "COU.AG.PMD",
      "type": "number"
    },
    {
      "allowedValues": {
        "values": [
          "NONE",
          "DEFAULT",
          "AUTO_GUIDE",
          "ADAPT_OPT",
          "ADAPT_OPT_TCCD",
          "IR_AO_OFFAXIS",
          "IR_AO_ONAXIS1"
        ]
      },
      "default": "ADAPT_OPT",
      "label": "Type of Coude guiding",
      "minihelp": "Type of Coude guiding",
      "name": "COU.AG.TYPE",
      "type": "keyword"
    },
    {
      "allowedValues": {
        "values": [
          "small",
          "medium",
          "large",
          "astrometric",
          "UTs"
        ]
      },
      "label": "Interferometric Array",
      "minihelp": "List of interferometric arrays: first choice and possible alternatives",
      "name": "ISS.BASELINE",
      "type": "keywordlist"
    },
    {
      "allowedValues": {
        "values": [
          "snapshot",
          "imaging",
          "time-series",
          "astrometry"
        ]
      },
      "label": "Types of interferometric observations",
      "minihelp": "Types of interferometric observations",
      "name": "ISS.VLTITYPE",
      "type": "keywordlist"
    }
  ],
  "templateName": "GRAVITY_single_acq",
  "type": "acquisition"
}

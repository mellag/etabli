#for i in GRAVITY*acquisition; do echo $i; cat $i  | jq ".parameters[] | {(.name): {"list": .allowedValues.values}}" |grep BASELINE -A10  ; done



#for instrument in MATISSE
#for instrument in GRAVITY
for instrument in PIONIER GRAVITY MATISSE
do

  # get list of periods 
  #curl https://www.eso.org/copdemo/api/v1/instrumentPackages/${instrument}/
  PERIODS="104.05  105.13 105.14"
  # TODO HERE get last version from prev url

  # loop on some periods
  for period in $PERIODS 
  do
    PDIR=P${period}
    test -d $PDIR || mkdir -p $PDIR

    TEMPLATESIGNATURES=$PDIR/${instrument}.templateSignatures
    # retrieve list of templates
    test -e $TEMPLATESIGNATURES || wget  https://www.eso.org/copdemo/api/v1/instrumentPackages/${instrument}/${period}/templateSignatures -O $TEMPLATESIGNATURES

    # and loop on templates one by one
    TEMPLATES="$(cat $TEMPLATESIGNATURES | jq -r '.[] .templateName ')"
    for tmpl in $TEMPLATES
    do
      test -e $PDIR/${tmpl} || curl https://www.eso.org/copdemo/api/v1/instrumentPackages/${instrument}/${period}/templateSignatures/${tmpl} | jq --sort-keys . > $PDIR/${tmpl}
    done

    INSCONFILE=$PDIR/${instrument}.instrumentConstraints
    test -e $INSCONFILE || curl https://www.eso.org/copdemo/api/v1/instrumentPackages/${instrument}/${period}/instrumentConstraints | jq --sort-keys ".observingConstraints |= sort_by(.name)" > $INSCONFILE

  done

done

jq -r 'group_by(.filename) | .[] | unique_by(.diameter) | select(length  >=2 ) | .[] | "|" + .target + "|" + .fitter_name + "|" + (.diameter | tostring) + "|" + (.reduced_chi2 | tostring) + "|" + (.run_duration | tostring) + "|" + .filename + "|"' batch-calliper-2022-02-08.json 

| target | fitter_name | diameter | reduced_chi2 | run_duration | filename | 
|--------|-------------|----------|--------------|--------------|----------|
|MIR0001|trfit|34.1173|356.248|0|2006-03-04.fits|
|MIR0001|genfit|43.6125|354.991|6|2006-03-04.fits|
|HDN163376|trfit|0|3.25107e-13|0|2007-06-29.fits|
|HDN163376|genfit|1.71132e-08|3.25107e-13|0|2007-06-29.fits|
|HD56022|trfit|0.0186302|1.42532|0|2012-03-24_CAL_HD56022_oiDataCalib.fits|
|HD56022|genfit|0.0186303|1.42532|1|2012-03-24_CAL_HD56022_oiDataCalib.fits|
|HD97048|trfit|68.6872|25.848|0|2019-05-14T234940_HD97048_IR-N_LOW_IN_IN_noChop_cal_oifits_0.fits|
|HD97048|genfit|127.163|11.8804|4|2019-05-14T234940_HD97048_IR-N_LOW_IN_IN_noChop_cal_oifits_0.fits|
|HD197635|trfit|0|144.981|0|MERGE_BCDIN_RAW_VIS2_CORRFLUX_CAL_MATIS.04_01_TARGET_RAW_SIM_01b.fits|
|HD197635|genfit|1000|144.981|0|MERGE_BCDIN_RAW_VIS2_CORRFLUX_CAL_MATIS.04_01_TARGET_RAW_SIM_01b.fits|
|HD100546|trfit|3.00812|1497.78|0|PIONIER.2012-03-25T05p56p32.732_oidata.fits|
|HD100546|genfit|5.17169|1425.46|1|PIONIER.2012-03-25T05p56p32.732_oidata.fits|
|HD100546|trfit|2.75555|1999.01|0|PIONIER.2012-03-25T06p04p34.997_oidata.fits|
|HD100546|genfit|5.88656|1510.96|1|PIONIER.2012-03-25T06p04p34.997_oidata.fits|
|Canopus|trfit|7.23246|48.8728|0|PRODUCT_Canopus_1.89-2.39micron_2009-12-25T02_18_52.0616.fits|
|Canopus|genfit|24.8529|914.765|3|PRODUCT_Canopus_1.89-2.39micron_2009-12-25T02_18_52.0616.fits|
|HD87643|genfit|8.49199|157.906|1|PRODUCT_HD87643_1.54-1.87micron_2008-03-12T00:24:20.3943.fits|
|HD87643|trfit|8.492|157.906|0|PRODUCT_HD87643_1.54-1.87micron_2008-03-12T00:24:20.3943.fits|
|HD87643|genfit|6.66631|215.495|1|PRODUCT_HD87643_1.54-1.88micron_2008-03-06T07:02:20.6815.fits|
|HD87643|trfit|6.66632|215.495|0|PRODUCT_HD87643_1.54-1.88micron_2008-03-06T07:02:20.6815.fits|
|HD87643|trfit|7.79129|404.397|0|PRODUCT_HD87643_1.96-2.43micron_2008-03-06T05:05:53.7079.fits|
|HD87643|genfit|11.5808|374.433|2|PRODUCT_HD87643_1.96-2.43micron_2008-03-06T05:05:53.7079.fits|
|R_CAR|trfit|10.2277|8185.33|0|R_CAR_all.fits|
|R_CAR|genfit|33.9695|565.717|24|R_CAR_all.fits|
|T1ORIC|genfit|3.72988|156.61|1|Theta1Ori2007Dec03_2.fits|
|T1ORIC|trfit|3.72989|156.61|0|Theta1Ori2007Dec03_2.fits|
|T1ORIC|genfit|3.72988|156.61|1|Theta1Ori2007Dec03_2.image-oi.fits|
|T1ORIC|trfit|3.72989|156.61|1|Theta1Ori2007Dec03_2.image-oi.fits|
|VY_CMA|trfit|11.2014|4962.49|0|VY_CMA_all.fits|
|VY_CMA|genfit|18.4987|340.717|9|VY_CMA_all.fits|

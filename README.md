
| AI | CreationDate | Action Item | Actionee |  Due Date | Priority | Status | Closure Date |
|----|--------------|-------------|----------|-----------|----------|--------|--------------|
|----|--------------|**Développement du NSS**|----------|-----------|----------|--------|--------------|
|[18](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/18)|2021-09-06|Définition des noms des colonnes du catalogue SPICA|David Salabert|2021-09-10||Closed||2021-09-22
|[29](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/29)|2021-09-06|Fournir le liste des headers de fichiers SPICA pour préparer ingestion dans ObsPortal|David Salabert|2021-09-30||||
|[28](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/28)|2021-09-06|Documenter le protocole d’accès à ObsPortal|Laurent Bourges|2021-09-30||||
|[25](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/25)|2021-09-06|Développer nss_obs.py pour compléter les champs de la VO-table|David Salabert|2021-09-30||||
|[23](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/23)|2021-09-06|Tester/Valider le mode vectoriel des setups d'observation|Denis Mourard|2021-09-30||Closed||2021-09-16
|[22](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/22)|2021-09-06|Consolider les filtres de priorité du NSS|David Salabert|2021-09-30||Closed||2021-09-20
|[21](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/21)|2021-09-06|Créer un exemple de modification d'un champ de la table (API-write)|David Salabert|2021-09-30||Closed||2021-09-20
|[20](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/20)|2021-09-06|Interfacer nss_obs.py avec la table SPICA-DB de la démo|David Salabert|2021-09-30||Closed||2021-09-23
|[30](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/30)|2021-09-06|Intégrer l'algorithme de priorité dans nss_obs.py|David Salabert|2021-10-31||Closed||2021-09-20
|[27](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/27)|2021-09-06|Développer un prototype du QCS pouvant mettre à jour les champs de la table|David Salabert|2021-10-31||||
|[26](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/26)|2021-09-06|Tester et documenter l'ensemble des requêtes initiales de la table|Denis Mourard|2021-10-31||||
|[24](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/24)|2021-09-06|Gestion des observations dégradées|Denis Mourard|2021-10-31||||
|[19](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/19)|2021-09-06|Adaptation du catalogue SPICA au nouveau format|Nicolas Nardetto|2021-10-31||||
|[17](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/17)|2021-09-06|Accès utilisateurs à SPICA-DB: droits, groupes|Guillaume Mella|2021-10-31||||
|[31](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/31)|2021-09-06|Tester et documenter la capacité de programmer correctement le survey SPICA|Denis Mourard|2021-11-30||||
|[48](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/48)|2021-09-23|Gestion des étoiles pas observables en 6T||TBD||||
|[15](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/15)|2021-09-10|Format des VO tables pour ASPRO2||TBD||Closed||2021-09-20
|[12](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/12)|2021-02-02|ASPRO2: import targets as VOTable||TBD||||
|----|--------------|**V0**|----------|-----------|----------|--------|--------------|
|[11](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/11)|2021-02-02|ASPRO2: export targets as VOTable||TBD||Closed||2021-09-20
|[9](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/9)|2021-01-25|Organisation des données / spécifique/commun|Guillaume Mella|TBD||Closed||2021-09-20
|[6](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/6)|2021-01-12|Interrogation de la base - mode basique|Laurent Bourges|TBD||Closed||2021-09-20
|[3](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/3)|2021-01-12|Mise à jour de la base SURVEY||TBD||Closed||2021-09-20
|[2](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/2)|2021-01-12|Remplissage initial|Guillaume Mella|TBD||Closed||2021-09-20
|[1](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/1)|2021-01-12|Gestion des utilisateurs||TBD||Closed||2021-09-17
|----|--------------|**Gestion des calibrateurs**|----------|-----------|----------|--------|--------------|
|[33](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/33)|2021-09-07|Décision puis implémentation pour les calibrateurs secondaires (JSDC)||2021-11-30||||
|[32](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/32)|2021-09-07|Rajouter la table des calibrateurs dans la base SPICA-DB||2021-11-30||||
|[34](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/34)|2021-09-07|Deuxième phase du test du survey complet SPICA + documentation||2021-12-31||||
|----|--------------|**Fonctionnalités des outils génériques JMMC et SPICA**|----------|-----------|----------|--------|--------------|
|[38](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/38)|2021-09-07|Créer l'hébergement des fichiers OIFITS SPICA sur serveur SPICA pour OiDB|David Salabert|2021-10-31||||
|[40](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/40)|2021-09-07|Réfléchir à l'alimentation semi-auto de BadCal||2022-03-31||||
|[39](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/39)|2021-09-07|Interfacer SPICA-DRS/QCS et Table SPICA-DB/OiDB/ObsPortal||2022-03-31||||
|[35](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/35)|2021-09-07|Ajouter les modèles géométriques dans les VO-tables||2022-03-31||||
|[41](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/41)|2021-09-07|Gestion du versioning des sorties DRS/QCS en fonction versioning DRS/QCS||TBD||||
|[37](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/37)|2021-09-07|Description de l'hébergement d'une collection d'OIFITS (+pdf) pour OiDB||TBD||||
|[36](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/36)|2021-09-07|Adaptation ASPRO2 si besoin nouveaux apparaissent||TBD||||
|[13](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/13)|2021-02-02|ASPRO2: import searchcal / getstar targets as VOTable||TBD||||
|----|--------------|**Mise en production**|----------|-----------|----------|--------|--------------|
|[44](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/44)|2021-09-07|Développer les protections en lecture pour SPICA-DB|Denis Mourard|2021-09-30||||
|[45](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/45)|2021-09-07|Développer une interface utilisateur pour NSS||2021-12-31||||
|[43](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/43)|2021-09-07|Développer SPICA-DB en multi-tables relationnelles||2021-12-31||||
|[42](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/42)|2021-09-07|Définir l'architecture générale et les relations entre applications||2021-12-31||||
|[46](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/46)|2021-09-07|Développer l'outil d'alimentation/consultation de SPICA-DB||2022-05-31||||
|[47](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/47)|2021-09-07|Vérifier les interfaces entre SPICA-SAS et la table SPICA-DB||TBD||||
|[14](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/14)|2021-03-25|Création du formulaire de saisie PI||TBD||||
|[10](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/10)|2021-02-02|ASPRO2: amélioration BEST PoPs algorithm||TBD||||
|[5](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/5)|2021-01-12|Consultation||TBD||||
|[4](https://gitlab.oca.eu/SPICA_GRP/spica-db/-/issues/4)|2021-01-12|Remplissage base ""Proposals temps ouvert""||TBD||||"


```mermaid

gantt
    dateFormat  YYYY-MM-DD
    title       SPICA-DB
    excludes    weekends
    
    
    section Développement du NSS(18)
    Gestion des étoiles pas observables en 6T : t_105_1, 2021-09-23, 1d
    Format des VO tables pour ASPRO2 :done t_105_2, after t_105_1, 1d
    Tester et documenter la capacité de programmer correctement le survey SPICA : t_105_3, after t_105_2, 1d
    Intégrer l'algorithme de priorité dans nss_obs.py :done t_105_4, after t_105_3, 1d
    Fournir le liste des headers de fichiers SPICA pour préparer ingestion dans ObsPortal : t_105_5, after t_105_4, 1d
    Documenter le protocole d’accès à ObsPortal : t_105_6, after t_105_5, 1d
    Développer un prototype du QCS pouvant mettre à jour les champs de la table : t_105_7, after t_105_6, 1d
    Tester et documenter l'ensemble des requêtes initiales de la table : t_105_8, after t_105_7, 1d
    Développer nss_obs.py pour compléter les champs de la VO-table : t_105_9, after t_105_8, 1d
    Gestion des observations dégradées : t_105_10, after t_105_9, 1d
    Tester/Valider le mode vectoriel des setups d'observation :done t_105_11, after t_105_10, 1d
    Consolider les filtres de priorité du NSS :done t_105_12, after t_105_11, 1d
    Créer un exemple de modification d'un champ de la table (API-write) :done t_105_13, after t_105_12, 1d
    Interfacer nss_obs.py avec la table SPICA-DB de la démo :done t_105_14, after t_105_13, 1d
    Adaptation du catalogue SPICA au nouveau format : t_105_15, after t_105_14, 1d
    Accès utilisateurs à SPICA-DB/ droits, groupes : t_105_16, after t_105_15, 1d
    Définition des noms des colonnes du catalogue SPICA :done t_105_17, after t_105_16, 1d
    ASPRO2/ import targets as VOTable : t_105_18, after t_105_17, 1d
    
    section Gestion des calibrateurs(3)
    Deuxième phase du test du survey complet SPICA + documentation : t_106_1, 2021-09-07, 1d
    Décision puis implémentation pour les calibrateurs secondaires (JSDC) : t_106_2, after t_106_1, 1d
    Rajouter la table des calibrateurs dans la base SPICA-DB : t_106_3, after t_106_2, 2021-11-30
    
    
    section Fonctionnalités des outils génériques JMMC et SPICA(8)
    Gestion du versioning des sorties DRS/QCS en fonction versioning DRS/QCS : t_107_1, 2021-09-07, 1d
    Réfléchir à l'alimentation semi-auto de BadCal : t_107_2, after t_107_1, 1d
    Interfacer SPICA-DRS/QCS et Table SPICA-DB/OiDB/ObsPortal : t_107_3, after t_107_2, 1d
    Créer l'hébergement des fichiers OIFITS SPICA sur serveur SPICA pour OiDB : t_107_4, after t_107_3, 1d
    Description de l'hébergement d'une collection d'OIFITS (+pdf) pour OiDB : t_107_5, after t_107_4, 1d
    Adaptation ASPRO2 si besoin nouveaux apparaissent : t_107_6, after t_107_5, 1d
    Ajouter les modèles géométriques dans les VO-tables : t_107_7, after t_107_6, 1d
    ASPRO2/ import searchcal / getstar targets as VOTable : t_107_8, after t_107_7, 1d
    
    
    section Mise en production(10)
    Vérifier les interfaces entre SPICA-SAS et la table SPICA-DB : t_108_1, 2021-09-07, 1d
    Développer l'outil d'alimentation/consultation de SPICA-DB : t_108_2, after t_108_1, 1d
    Développer une interface utilisateur pour NSS : t_108_3, after t_108_2, 1d
    Développer les protections en lecture pour SPICA-DB : t_108_4, after t_108_3, 1d
    Développer SPICA-DB en multi-tables relationnelles : t_108_5, after t_108_4, 1d
    Définir l'architecture générale et les relations entre applications : t_108_6, after t_108_5, 1d
    Création du formulaire de saisie PI : t_108_7, after t_108_6, 1d
    ASPRO2/ amélioration BEST PoPs algorithm : t_108_8, after t_108_7, 1d
    Consultation : t_108_9, after t_108_8, 1d
    Remplissage base ""Proposals temps ouvert"" : t_108_10, after t_108_9, 1d
       
```


# First Readme mainly to test gitlab editing capabilities.

```plantuml
Bob -> Alice : hello
Alice -> Bob : hi
```


```mermaid
gantt
        dateFormat  YYYY-MM-DD
        title Todo Liste JMMC non ordonnée des actions demandées et acceptées
        
        section RoadMap
        MAJ todolist            :done,    des1, 2019-07-01, 1h
        
        section JMDC
        dev d'une interface d'edition web collaborative :30d
        
        section A2P2
        Gestion asynchrone d'un lot d'OB :15d
        Gestion dynamique des repertoires :15d
        Ergonomie (préférences / liens cliquables vers p2 ESO) :15d
        
        section Proto portail utilisateur JMMC / porte-feuille d'observation (6 mois CDD)
        Gestion d'un porte-feuille d'observation collaboratif pour croiser préparation et suivi des programmes d'observation :90d
        Affichage dans Aspro des observations déjà réalisées :120d
        Liste gérée à la main :30d
        Infos récupérées de l'archive ESO automatiquement (L0 + configuration baselines et instrumentale) :30d
        
        section LITpro
        Passage webservice en mode asynchrone :30d 
        ...en version de course (K8S) :60d
        Récupération des resultats en OIFits (modèle imageOI) :30d
        MAJ du module de gestion des OIFits (vieux code de 15ans vers OiTools) plot et traitements :60d
        Amélioration de l'érgonomie (resultats):30d
        Portail modèles utilisateurs :60d
        
        section OImaging
        Faciliter la génération de l'image de départ (depuis LITpro /  images simples) :30d
        Mettre en place la notion de fichier de session .oimaxml ( garder/reprendre d'anciens jobs en cours d'execution + sauvegarde des resultats) :30d
        Suivi direct de l'execution à distance (log+images intermediaires) :15d
        Transposition du mode d'execution serveur docker sur kubernates (K8S) cf LITpro :30d

        
        section SearchCal
        MAJ code simcli/telnet -> TapVizier & SimbadTap :60d
        Evolution pour intégration catalogue MATISSE et GAIA :60d
        
        section JSDC
        V2.1 pour inclure les informations pertinentes du catalogue MATISSE (P. Cruzalebes), GAIA DR2 (positions, proper motion, magnitudes) dans JSDC 2 :60d
        
        section OITools / OIFits Explorer
        Amélioration OIFITS merger / selection (dé-duplication des tables identiques) :30d
        Gestion fine des targets ( compagnon, pb précision coord, PM, ....) :30d
        Filtrage avancé (plage de longueur d'ondes) :30d
        
        section OiDB
        Support de collections privées / partagées (SUV/collaborations) :60d
        
        section Aspro 2
        Transformée de Hankel (profils stellaires 1D) :30d
        Intégration des modèles AMHRA comme modèles utilisateurs :30d

```
```mermaid
gantt
        dateFormat  YYYY-MM-DD
        title Todo Liste JMMC non ordonnée des actions prospectives
        
        section RoadMap
        MAJ todolist            :done,    des1, 2022-07-01, 1h
                section Proto portail utilisateur JMMC / porte-feuille d'observation (6 mois CDD)
        section LITpro
        SED :30d
        Support modèles AMHRA :60d
        
        section OImaging
        Support multi-fichiers :20d
        Outils de comparaisons des résultats (comparaison d'images, chi2) :60d
        intégration des logiciels polychromatiques (Painter) :30d
        intégration des logiciels "hors JMMC" Irbis/Squeeze : 30d

        
        section A2P2
        section SearchCal
        section JSDC
        V3 MAJ de l'ensemble JMDC / JSDC avec l'ensemble SIMBAD x GAIA (~4.5 millions d'étoiles) + nouveau polynomes avec mags G, Bp, Rb :90d
        
        section OITools / OIFits Explorer
        Valider le support des observables OIFITS V2 :60d
        Transformation :30d
        
        
        section OiDB
        Support OIFITS2 :60d
        Liens L0-L2/3 :30d
        
        section Aspro 2
        
        section JMDC
        

```

--        
